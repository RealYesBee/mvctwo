package org.yesbee.demo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

public class StudentDBUtil {
	
	private DataSource datasource;

	public StudentDBUtil(DataSource thedatasource) {
		datasource = thedatasource;
	}
	
	public List<Student> getStudents() throws Exception{
		
		List<Student> students = new ArrayList<Student>();
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRS = null;
		
		try{
			
			myConn = datasource.getConnection();
			
			String sql = "Select * from student order by last_name";
			myStmt = myConn.createStatement();
			
			myRS = myStmt.executeQuery(sql);
			
			while(myRS.next())
			{
				int id = myRS.getInt("id");
				String firstName = myRS.getString("first_name");
				String lastName = myRS.getString("last_name");
				String eMail = myRS.getString("email");
				
				Student tempStudent = new Student(id,firstName,lastName,eMail);
				students.add(tempStudent);
			}
			
			return students;
		} finally{
			close(myConn,myStmt,myRS);
		}
		
		
		
		
	}
	
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		Connection myConn = null;
		PreparedStatement pstmt = null;
		
		try{
			
			myConn = datasource.getConnection();
			
			String sql = "insert into student (first_name,last_name,email) values(?,?,?)";
			pstmt = myConn.prepareStatement(sql);
			
			pstmt.setString(1, student.getFirstName());
			pstmt.setString(2, student.getSecondName());
			pstmt.setString(3, student.geteMail());
			
			pstmt.execute();
			
		}catch(Exception exe){
			exe.printStackTrace();
		}finally {
			close(myConn,pstmt,null);
		}
		
	}

	private void close(Connection myConn, Statement myStmt, ResultSet myRS) {
		// TODO Auto-generated method stub
		
		try{
			if(myRS != null){
				myRS.close();
			}
			
			if(myStmt !=null){
				myStmt.close();
			}
			if(myConn !=null){
				myConn.close();
			}
		}
		catch(Exception exe){
			exe.printStackTrace();
			
		}
		
	}

	public List<Student> getStudentByID(int studentid) throws Exception {
		
		List<Student> students = new ArrayList<Student>();
		Connection myConn = null;
		PreparedStatement pstmt = null;
		ResultSet myRS = null;
		
		try{
			
			myConn = datasource.getConnection();
			
			String sql = "Select * from student where id =?";
			pstmt = myConn.prepareStatement(sql);
			pstmt.setInt(1, studentid);
			
			myRS = pstmt.executeQuery();
			
			while(myRS.next())
			{
				int id = myRS.getInt("id");
				String firstName = myRS.getString("first_name");
				String lastName = myRS.getString("last_name");
				String eMail = myRS.getString("email");
				
				Student tempStudent = new Student(id,firstName,lastName,eMail);
				students.add(tempStudent);
			}
			
			
		} finally{
			close(myConn,pstmt,myRS);
		}
		return students;
	}

	public void updateStudent(Student upStudent) {
		
		Connection myConn = null;
		PreparedStatement pstmt = null;
		
		try{
			
			myConn = datasource.getConnection();
			
			String sql = "update student "
					+ "SET first_name=?,last_name=?,email=? "
					+ "where id=?" ;
			pstmt = myConn.prepareStatement(sql);
			
			pstmt.setString(1, upStudent.getFirstName());
			pstmt.setString(2, upStudent.getSecondName());
			pstmt.setString(3, upStudent.geteMail());
			pstmt.setInt(4, upStudent.getId());
			
			pstmt.execute();
			
		}catch(Exception exe){
			exe.printStackTrace();
		}finally {
			close(myConn,pstmt,null);
		}

		
	}

	public void deleteStudent(int studentid) {
		
		Connection myConn = null;
		PreparedStatement pstmt = null;
		
		try{
			
			myConn = datasource.getConnection();
			
			String sql = "delete from student where id=?";
			pstmt = myConn.prepareStatement(sql);
			
			pstmt.setInt(1, studentid);
			
			pstmt.execute();
			
		}catch(Exception exe){
			exe.printStackTrace();
		}finally {
			close(myConn,pstmt,null);
		}
		
	}

	

}
