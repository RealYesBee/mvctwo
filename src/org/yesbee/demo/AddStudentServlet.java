package org.yesbee.demo;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class AddStudentServlet
 */
@WebServlet("/AddStudentServlet")

public class AddStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private StudentDBUtil sdbUtil;
    @Resource(name="jdbc/web_student_tracker")
    private DataSource datasource;

	@Override
	public void init() throws ServletException {
		
		super.init();
		try {
			 sdbUtil = new StudentDBUtil(datasource);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			addStudent(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String eMail = request.getParameter("email");
		
		Student student = new Student(firstName, lastName, eMail);
		sdbUtil.addStudent(student);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/add_student_form.jsp");
		dispatcher.forward(request, response);
		
	}

}
