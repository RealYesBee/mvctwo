package org.yesbee.demo;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class LoadStudent
 */
@WebServlet("/LoadStudent")
public class LoadStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    private StudentDBUtil sdbUtil;
    @Resource(name="jdbc/web_student_tracker")
    private DataSource datasource;
	
    public void init() throws ServletException {
		
		super.init();
		try {
			 sdbUtil = new StudentDBUtil(datasource);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String eMail = request.getParameter("email");
		int id = Integer.parseInt(request.getParameter("studentID"));
		
		Student upStudent = new Student(id, firstName, lastName, eMail);
		sdbUtil.updateStudent(upStudent);
		
		RequestDispatcher dis = request.getRequestDispatcher("StudentServlet");
		dis.forward(request, response);
		
	}

}
