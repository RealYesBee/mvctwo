package org.yesbee.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


/**
 * Servlet implementation class ServletTest
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private StudentDBUtil sdbUtil;
    @Resource(name="jdbc/web_student_tracker")
    private DataSource datasource;

	@Override
	public void init() throws ServletException {
		
		super.init();
		try {
			 sdbUtil = new StudentDBUtil(datasource);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			listStudents(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


	private void listStudents(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		
		List<Student> students = sdbUtil.getStudents();
		request.setAttribute("list_students", students);
		RequestDispatcher dispathcer = request.getRequestDispatcher("/view_students.jsp");
		dispathcer.forward(request, response);
	}

}
