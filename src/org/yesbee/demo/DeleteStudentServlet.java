package org.yesbee.demo;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class DeleteStudentServlet
 */
@WebServlet("/DeleteStudentServlet")
public class DeleteStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private StudentDBUtil sdbUtil;
    @Resource(name="jdbc/web_student_tracker")
    private DataSource datasource;

	@Override
	public void init() throws ServletException {
		
		super.init();
		try {
			 sdbUtil = new StudentDBUtil(datasource);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int Studentid = Integer.parseInt(request.getParameter("StudentID"));
		try {
			sdbUtil.deleteStudent(Studentid);
			RequestDispatcher dispatcher = request.getRequestDispatcher("StudentServlet");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
