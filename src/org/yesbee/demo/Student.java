package org.yesbee.demo;

public class Student 
{
	private int id;
	private String firstName;
	private String secondName;
	private String eMail;
	
	public Student(int id, String firstName, String secondName, String eMail) {
		this.id = id;
		this.firstName = firstName;
		this.secondName = secondName;
		this.eMail = eMail;
	}

	public Student(String firstName, String secondName, String eMail) {
		this.firstName = firstName;
		this.secondName = secondName;
		this.eMail = eMail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", secondName=" + secondName + ", eMail=" + eMail
				+ "]";
	}
	
	
	
	
	

}
