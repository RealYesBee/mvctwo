<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,org.yesbee.demo.*" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FooBar University</title>
</head>
<body>

<%

	List<Student> theStudents = (List<Student>) request.getAttribute("list_students");
%>
    <div id="wrapper">
    <div id="header">
    <h3> Foobar University </h3>
    </div>
    </div>
    
     <div id="container">
     <div id="content">
     <form action="add_student_form.jsp">
      <input type="submit" value="Add Student">
     </form>
     <table border=1>
     <tr>
     	<th>First Name</th>
     	<th>Last Name </th>
     	<th> Email </th>
     	<th colspan="2">Take Action</th>
     </tr>
     
     <c:forEach var="tempStudents" items="${list_students}">
     <c:url var="tempLink" value="UpdateStudentServlet">
		<c:param name="StudentID" value="${tempStudents.id}"/>
	</c:url>
	<c:url var="tempLink1" value="DeleteStudentServlet">
		<c:param name="StudentID" value="${tempStudents.id}"/>
	</c:url>
     <tr>
		  <td> ${tempStudents.firstName} </td>
		  <td> ${tempStudents.secondName} </td>
		  <td> ${tempStudents.eMail} </td>
		  <td> <a href="${tempLink}">Update</a> </td>
		  <td> <a href="${tempLink1}">Delete</a></td>
	</tr>
     </c:forEach>
     
     </table>
     </div>
     </div>




</body>
</html>